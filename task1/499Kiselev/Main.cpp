#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

#include <iostream>
#include <vector>


glm::vec3 kuenPoint(float radius, float v, float u) {
    float denom = cosh(v) * cosh(v) + u * u;
    float x = radius * 2 * cosh(v) * (cos(u) + u * sin(u)) / denom;
    float y = radius * 2 * cosh(v) * (- u * cos(u) + sin(u)) / denom;
    float z = radius * (v - 2 * sinh(v) * cosh(v) / denom);
    return glm::vec3(x, y, z);
}


glm::vec3 kuenNormal(float radius, float v, float u) {
    float eps = 1e-5;
    auto r = kuenPoint(radius, v, u);
    auto r_plus_u = kuenPoint(radius, v, u + eps);
    auto r_plus_v = kuenPoint(radius, v + eps, u);
    
    auto ru = (r_plus_u - r) / eps;
    auto rv = (r_plus_v - r) / eps;

    return glm::normalize(glm::cross(ru, rv));
}


MeshPtr makeKuen(float size, int detalization, float min_u, float max_u, float min_v, float max_v) {
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;

    for (int i = 0; i < detalization; ++i) {
        float u1 = min_u + (max_u - min_u) * i / detalization;
        float u2 = min_u + (max_u - min_u) * (i + 1) / detalization;
        for (int j = 0; j < detalization; ++j) {
            float v1 = min_v + (max_v - min_v) * j / detalization;
            float v2 = min_v + (max_v - min_v) * (j + 1) / detalization;
        
            vertices.push_back(kuenPoint(size, u1, v1));
            vertices.push_back(kuenPoint(size, u1, v2));
            vertices.push_back(kuenPoint(size, u2, v1));

            normals.push_back(kuenNormal(size, u1, v1));
            normals.push_back(kuenNormal(size, u1, v2));
            normals.push_back(kuenNormal(size, u2, v1));

            vertices.push_back(kuenPoint(size, u2, v1));
            vertices.push_back(kuenPoint(size, u1, v2));
            vertices.push_back(kuenPoint(size, u2, v2));

            normals.push_back(kuenNormal(size, u2, v1));
            normals.push_back(kuenNormal(size, u1, v2));
            normals.push_back(kuenNormal(size, u2, v2));
        }
    }

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    // std::cout << "Kuen surface is created with " << vertices.size() << " vertices\n";

    return mesh;
}


class SampleApplication : public Application
{
public:
    MeshPtr _kuen;

    ShaderProgramPtr _shader;
    float _min_u = -glm::pi<float>();
    float _max_u = glm::pi<float>();
    float _min_v = -glm::pi<float>();
    float _max_v = glm::pi<float>();
    int _detalization = 100;

    void makeScene() override {
        Application::makeScene();

        updateKuen();

        _shader = std::make_shared<ShaderProgram>("499KiselevData/shader.vert", "499KiselevData/shader.frag");
    }

    void update() override {
        Application::update();
        updateKuen();
    }

    void updateKuen() {
        _kuen = makeKuen(1, _detalization, _min_u, _max_u, _min_v, _max_v);

    }

    void updateGUI() override {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL", NULL, ImGuiWindowFlags_AlwaysAutoResize)) {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);
            ImGui::Text("detalization %d", _detalization);

            ImGui::SliderFloat("min_u", &_min_u, -3 * glm::pi<float>(), 3 * glm::pi<float>());
            ImGui::SliderFloat("max_u", &_max_u, -3 * glm::pi<float>(), 3 * glm::pi<float>());
            ImGui::SliderFloat("min_v", &_min_v, -3 * glm::pi<float>(), 3 * glm::pi<float>());
            ImGui::SliderFloat("max_v", &_max_v, -3 * glm::pi<float>(), 3 * glm::pi<float>());
        }
        ImGui::End();
    }

    void handleKey(int key, int scancode, int action, int mods) override {
        Application::handleKey(key, scancode, action, mods);
        if (action == GLFW_PRESS) {
            if (key == GLFW_KEY_EQUAL) {
                _detalization += 5;
                updateKuen();
            }

            if (key == GLFW_KEY_MINUS) {
                _detalization -= 5;
                _detalization = std::max(_detalization, 5);
                updateKuen();
            }
        }
    }

    void draw() override
    {
        Application::draw();

        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдер
        _shader->use();

        //Загружаем на видеокарту значения юниформ-переменные: время и матрицы
        _shader->setFloatUniform("time", (float)glfwGetTime()); //передаем время в шейдер

        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
        _shader->setMat4Uniform("modelMatrix", _kuen->modelMatrix());
        _kuen->draw();
    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}
